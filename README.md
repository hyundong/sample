# README #

### View간의 데이터 호출 및 사용 ###
* UICollectionView Delegate, DataSource
* UICollectionView DiffableDataSource 
* ViewModel 활용
* Delegation의 이해
* DependencyInjection 
https://www.martinfowler.com/articles/injection.html
* Boundary 
    
### 앱 메모리 관리 (초기화, 자동 새로고침) ###
* Autoreference Count 
* Strong, weak, unowned
  
### 스레드 동기 / 비동기, 콜백 알림 ###
* DispatchQueue sync, async 
* Closure

### Apple Documentation ###
https://developer.apple.com/documentation/

### Apple CollectionView 예제 ###
https://developer.apple.com/documentation/uikit/views_and_controls/collection_views/implementing_modern_collection_views
    
### 책추천 ###
* CleanCode http://www.kyobobook.co.kr/product/detailViewKor.laf?mallGb=KOR&ejkGb=KOR&barcode=9788966260959

* CleanArchitecture   http://www.kyobobook.co.kr/product/detailViewKor.laf?ejkGb=KOR&mallGb=KOR&barcode=9788966262472&orderClick=LAG&Kc=

* App Architecture
    https://www.raywenderlich.com/books/advanced-ios-app-architecture
