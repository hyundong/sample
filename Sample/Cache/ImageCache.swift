//
//  ImageCache.swift
//  Sample
//
//  Created by Private on 2021/11/18.
//

import Foundation
import UIKit

class ImageCache {
    static let shared = ImageCache()
    
    private let cache = NSCache<NSString, UIImage>()
    
    func set(key: String, image: UIImage) {
        cache.setObject(image, forKey: key as NSString)
    }
    
    func image(key: String) -> UIImage? {
        return cache.object(forKey: key as NSString)
    }
}
