//
//  DIContainer.swift
//  Sample
//
//  Created by Private on 2021/11/16.
//

import Foundation
import UIKit

class DIContainer {
    let dataStore = DataStore()
    
    func makeRootViewController() -> UIViewController {
        return UINavigationController(rootViewController: makeBasicCollectionViewController())
    }
    
    func makeBasicCollectionViewController() -> UIViewController {
        let viewController = BasicCollectionViewController()
        viewController.items = dataStore.sampleItems()
        return viewController
    }
    
    func makeDiffableCollectionViewController() -> UIViewController {
        let viewController = DiffableDataSourceCollectionViewController()
        viewController.items = dataStore.sampleItems()
        return viewController
    }
    
    func makeImageCollectionViewController(async: Bool, useCache: Bool) -> UIViewController {
        let viewController = ImageCollectionViewController()
        viewController.items = dataStore.sampleImageItems()
        viewController.async = async
        viewController.useCache = useCache
        return viewController
    }
    
    func makeImageViewController(_ imageItemViewModel: ImageItemViewModel) -> ImageItemViewController {
        let viewController = ImageItemViewController()
        viewController.viewModel = imageItemViewModel
        return viewController
    }
}
