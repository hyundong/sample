//
//  DataStore.swift
//  Sample
//
//  Created by Private on 2021/11/16.
//

import Foundation

class DataStore {
    func sampleItems() -> [Item] {
        return [
            Item(identifier: "1", title: "Basic", type: .basicCollectionView),
            Item(identifier: "2", title: "Diffable", type: .diffableCollectionView),
            Item(identifier: "3", title: "Sync Image", type: .syncLoadImageCollectionView),
            Item(identifier: "4", title: "Sync Image with Cache", type: .syncLoadImageCollectionViewWithCache),
            Item(identifier: "5", title: "Async Image", type: .asyncLoadImageCollectionView),
            Item(identifier: "6", title: "Async Image with Cache", type: .asyncLoadImageCollectionViewWithCache),
        ]
    }
    
    func sampleImageItems() -> [ImageItem] {
        var items: [ImageItem] = []
        for i in 0 ..< 20 {
            items.append(ImageItem(identifier: "\(i)", url: "https://picsum.photos/200/300"))
        }
        return items
    }
}
