//
//  DiffableDataSourceCollectionViewController.swift
//  Sample
//
//  Created by Private on 2021/11/18.
//

import UIKit

final class DiffableDataSourceCollectionViewController: UIViewController {
    
    enum Section {
        case main
    }
    
    var items: [Item] = []
    var dataSource: UICollectionViewDiffableDataSource<Section, ItemCellViewModel>?
    
    private lazy var collectionView: UICollectionView = {
        let view = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = String(describing: type(of: self))
        configureViews()
        configureDataSource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func configureViews() {
        view.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    func configureDataSource() {
        let cellRegistration = UICollectionView.CellRegistration<ItemCell, ItemCellViewModel> { cell, indexPath, item in
            cell.bind(item)
        }
        
        dataSource = UICollectionViewDiffableDataSource<Section, ItemCellViewModel>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, itemIdentifier) -> UICollectionViewCell in
            return collectionView.dequeueConfiguredReusableCell(using: cellRegistration, for: indexPath, item: itemIdentifier)
        })
        
        var snapShot = NSDiffableDataSourceSnapshot<Section, ItemCellViewModel>()
        snapShot.appendSections([.main])
        let viewModels = items.map { ItemCellViewModel(item: $0) }
        snapShot.appendItems(viewModels, toSection: .main)
        dataSource?.apply(snapShot)
    }
}

extension DiffableDataSourceCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.item]
        Router(navigationController: navigationController).moveToViewController(item.type)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width, height: 60)
    }
}
