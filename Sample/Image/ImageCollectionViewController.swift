//
//  ImageCollectionViewController.swift
//  Sample
//
//  Created by Private on 2021/11/18.
//

import UIKit

final class ImageCollectionViewController: UIViewController {
    
    enum Section {
        case main
    }
    
    var items: [ImageItem] = []
    var dataSource: UICollectionViewDiffableDataSource<Section, ImageItemViewModel>?
    var async: Bool = false
    var useCache: Bool = false
    
    private lazy var collectionView: UICollectionView = {
        let view = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = String(describing: type(of: self))
        configureViews()
        configureDataSource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func configureViews() {
        view.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    func configureDataSource() {
        let cellRegistration = UICollectionView.CellRegistration<ImageItemCell, ImageItemViewModel> { cell, indexPath, item in
            cell.bind(item)
        }
        
        dataSource = UICollectionViewDiffableDataSource<Section, ImageItemViewModel>(collectionView: collectionView, cellProvider: { (collectionView, indexPath, itemIdentifier) -> UICollectionViewCell in
            return collectionView.dequeueConfiguredReusableCell(using: cellRegistration, for: indexPath, item: itemIdentifier)
        })
        
        var snapShot = NSDiffableDataSourceSnapshot<Section, ImageItemViewModel>()
        snapShot.appendSections([.main])
        let viewModels = items.map { ImageItemViewModel(imageItem: $0, async: self.async, useCache: self.useCache) }
        snapShot.appendItems(viewModels, toSection: .main)
        dataSource?.apply(snapShot)
    }
}

extension ImageCollectionViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.item]
        Router(navigationController: navigationController).moveToImageViewController(ImageItemViewModel(imageItem: item, async: true, useCache: true))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - 5)/2
        let height = width
        return .init(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
