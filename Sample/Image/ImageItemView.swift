//
//  ImageItemView.swift
//  Sample
//
//  Created by Private on 2021/11/19.
//

import UIKit

protocol ImageItemViewDelegate: AnyObject {
    func imageItemViewDidTouchButton(_ view: ImageItemView)
}

class ImageItemView: UIView {
    private let imageView: UIImageView = {
        let view = UIImageView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    
    private let actionButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Button", for: .normal)
        button.addTarget(self, action: #selector(didTouchButton), for: .touchUpInside)
        return button
    }()
    
    var actionHandler: (()->Void)?
    var viewModel: ImageItemViewModel?
    weak var delegate: ImageItemViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViews()
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureViews() {
        backgroundColor = .black
        
        addSubview(imageView)
        addSubview(actionButton)
        
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.heightAnchor.constraint(equalTo: widthAnchor),
            
            actionButton.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 50),
            actionButton.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
            actionButton.widthAnchor.constraint(equalToConstant: 200)
        ])
    }
    
    @objc
    private func didTouchButton() {
        print("didTouchButton")
        delegate?.imageItemViewDidTouchButton(self)
        actionHandler?()
    }
    
    func bind(_ viewModel: ImageItemViewModel) {
        self.viewModel = viewModel
        
        if let image = ImageCache.shared.image(key: viewModel.identifier) {
            imageView.image = image
        } else {
            URLSession.shared.dataTask(with: viewModel.url) { [weak self] data, response, error in
                DispatchQueue.main.async {
                    guard let data = data,
                          let image = UIImage(data: data) else {
                              return
                          }
                    
                    self?.imageView.image = image
                    ImageCache.shared.set(key: viewModel.identifier, image: image)
                }
            }
        }
    }
}
