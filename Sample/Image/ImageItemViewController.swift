//
//  ImageItemViewController.swift
//  Sample
//
//  Created by Private on 2021/11/18.
//

import Foundation
import UIKit

class ImageItemViewController: UIViewController {
    
    private lazy var imageItemView: ImageItemView = {
        let view = ImageItemView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        return view
    }()
    
    var viewModel: ImageItemViewModel?
        
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        configureBinding()
    }
    
    private func configureViews() {
        view.backgroundColor = .black
        view.addSubview(imageItemView)
        
        NSLayoutConstraint.activate([
            imageItemView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imageItemView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            imageItemView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            imageItemView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
    private func configureBinding() {
        guard let viewModel = viewModel else {
            return
        }
        imageItemView.bind(viewModel)
        imageItemView.actionHandler = {
            print("Called by Closure")
        }
    }
}

extension ImageItemViewController: ImageItemViewDelegate {
    func imageItemViewDidTouchButton(_ view: ImageItemView) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)

        present(alert, animated: true, completion: nil)
    }
}
