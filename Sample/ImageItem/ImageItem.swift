//
//  ImageItem.swift
//  Sample
//
//  Created by Private on 2021/11/18.
//

import Foundation

struct ImageItem: Hashable {
    let identifier: String
    let url: String
    
    static func == (lhs: ImageItem, rhs: ImageItem) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
