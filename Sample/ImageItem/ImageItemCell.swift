//
//  ImageItemCell.swift
//  Sample
//
//  Created by Private on 2021/11/18.
//

import Foundation
import UIKit

class ImageItemCell: UICollectionViewCell {
    
    private let imageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        return view
    }()
    
    private var itemViewModel: ItemCellViewModel?
    private var dataTask: URLSessionDataTask?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        dataTask?.cancel()
        imageView.image = nil
    }
    
    private func configureViews() {
        contentView.addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
        ])
    }
    
    func bind(_ viewModel: ImageItemViewModel) {
        if viewModel.useCache,
           let image = ImageCache.shared.image(key: viewModel.identifier) {
            imageView.image = image
            return
        }
        
        if viewModel.async {
            asyncLoadImage(viewModel)
        } else {
            loadImage(viewModel)
        }
    }
    
    func asyncLoadImage(_ viewModel: ImageItemViewModel) {
        dataTask = URLSession.shared.dataTask(with: viewModel.url) { [weak self] data, response, error in
            if let error = error {
                print("\(error.localizedDescription)")
                return
            }
            if let data = data {
                DispatchQueue.main.async {
                    let image = UIImage(data: data)!
                    self?.imageView.image = image
                    ImageCache.shared.set(key: viewModel.identifier, image: image)
                }
            }
        }
        dataTask?.resume()
    }
    
    func loadImage(_ viewModel: ImageItemViewModel) {
        if let data = try? Data(contentsOf: viewModel.url) {
            let image = UIImage(data: data)!
            imageView.image = image
            ImageCache.shared.set(key: viewModel.identifier, image: image)
        }
    }
}
