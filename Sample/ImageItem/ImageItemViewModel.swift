//
//  ImageItemCellViewModel.swift
//  Sample
//
//  Created by Private on 2021/11/18.
//

import Foundation

struct ImageItemViewModel: Hashable {
    
    let imageItem: ImageItem
    let async: Bool
    let useCache: Bool
    
    var identifier: String {
        return imageItem.identifier
    }
    
    var url: URL {
        URL(string: imageItem.url)!
    }
    
    static func == (lhs: ImageItemViewModel, rhs: ImageItemViewModel) -> Bool {
        return lhs.imageItem.identifier == rhs.imageItem.identifier
    }
}
