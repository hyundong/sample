//
//  Item.swift
//  Sample
//
//  Created by Private on 2021/11/16.
//

import Foundation

enum ItemType {
    case basicCollectionView
    case diffableCollectionView
    case syncLoadImageCollectionView
    case syncLoadImageCollectionViewWithCache
    case asyncLoadImageCollectionView
    case asyncLoadImageCollectionViewWithCache
}

struct Item: Hashable {
    let identifier: String
    let title: String
    let type: ItemType
    
    static func == (lhs: Item, rhs: Item) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}

