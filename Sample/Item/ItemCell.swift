//
//  ItemCell.swift
//  Sample
//
//  Created by Private on 2021/11/16.
//

import Foundation
import UIKit

class ItemCell: UICollectionViewCell {
    
    private let label: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .blue
        view.font = UIFont.systemFont(ofSize: 24)
        return view
    }()
    
    private var itemViewModel: ItemCellViewModel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureViews() {
        contentView.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
            label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
        ])
    }
    
    func bind(_ viewModel: ItemCellViewModel) {
        label.text = viewModel.title
    }
}
