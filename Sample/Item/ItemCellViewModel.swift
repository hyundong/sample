//
//  ItemViewModel.swift
//  Sample
//
//  Created by Private on 2021/11/16.
//

import Foundation

struct ItemCellViewModel: Hashable {
    let item: Item
    
    var title: String {
        item.title
    }
    
    static func == (lhs: ItemCellViewModel, rhs: ItemCellViewModel) -> Bool {
        return lhs.item.identifier == rhs.item.identifier
    }
}
