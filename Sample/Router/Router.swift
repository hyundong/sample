//
//  Router.swift
//  Sample
//
//  Created by Private on 2021/11/18.
//

import Foundation
import UIKit

class Router {
    let diContainer = DIContainer()
    weak var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
    
    func moveToViewController(_ itemType: ItemType) {
        switch itemType {
        case .basicCollectionView:
            let viewController = diContainer.makeBasicCollectionViewController()
            navigationController?.pushViewController(viewController, animated: true)
            
        case .diffableCollectionView:
            let viewController = diContainer.makeDiffableCollectionViewController()
            navigationController?.pushViewController(viewController, animated: true)
            
        case .syncLoadImageCollectionView:
            let viewController = diContainer.makeImageCollectionViewController(async: false, useCache: false)
            navigationController?.pushViewController(viewController, animated: true)
            
        case .syncLoadImageCollectionViewWithCache:
            let viewController = diContainer.makeImageCollectionViewController(async: false, useCache: true)
            navigationController?.pushViewController(viewController, animated: true)
            
        case .asyncLoadImageCollectionView:
            let viewController = diContainer.makeImageCollectionViewController(async: true, useCache: false)
            navigationController?.pushViewController(viewController, animated: true)
            
        case .asyncLoadImageCollectionViewWithCache:
            let viewController = diContainer.makeImageCollectionViewController(async: true, useCache: true)
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func moveToImageViewController(_ viewModel: ImageItemViewModel) {
        let viewController = diContainer.makeImageViewController(viewModel)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}
